#!/bin/bash

apt-get update
apt-get upgrade -y
apt-get install zsh vim tmux rsync curl git -y
rsync -av dotfiles/ ~/
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
sed -i 's/robbyrussell/darkblood/g' ~/.zshrc

