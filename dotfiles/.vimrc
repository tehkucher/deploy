set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'chase/vim-ansible-yaml'
Plugin 'vim-syntastic/syntastic'
Plugin 'indentpython.vim'
Plugin 'jnurmine/Zenburn'


call vundle#end()            " required
filetype plugin indent on

set tabstop=2
set shiftwidth=2
set autoindent

au BufRead, BugNewFile *.yml set filetype=yaml.ansible

let python_highlight_all=1
syntax on
set nonumber
set relativenumber
colors zenburn

set clipboard=unnamedplus,unnamed
